import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);

const opts = {
  theme: {
    themeDefault: "light",
    themes: {
      light: {
        background: "#8b3da6",
        primary: "#1867c0",
        success: "#00E676",
        secondary: "#b0bec5",
        accent: "#8c9eff",
        error: "#b71c1c",
        bgcard: "#472c5a",
        bg: "#350258",
        borderg: "#d2b371",
        bgs: "#1f0620",
        c: "#ffc105",
        "bg-w": "#fff",
        white: "#fff",
        "bg-box-number": "#E1F500",
      },
    },
  },
};

export default new Vuetify(opts);
