import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/pages/Home";
import Lottery from "@/pages/Lottery";
import Deposit from "@/pages/Deposit";
import LottoPlay from "@/pages/Lotto_play";
import Withdraw from "@/pages/Withdraw";
import Register from "@/pages/Register";
import Signin from "@/pages/Signin";
import store from "./store";
import Pingpong from "@/pages/Pingpong";
import PingpongPlay from "@/pages/Pingpong_play";
import Transection from "@/pages/Transection";
import LotteryView from "@/pages/Lotto_view";
import Awardresult from "@/pages/Awardresult";
import Profile from "@/pages/Profile";
import Favorite from "@/pages/Favorite";
import Contact from "@/pages/Contact";

Vue.use(VueRouter);

const routes = [
  {
    name: "",
    path: "/",
    component: Signin,
    meta: {
      guest: true,
    },
  },
  {
    name: "contact",
    path: "/contact",
    component: Contact,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "signin",
    path: "/signin",
    component: Signin,
    meta: {
      guest: true,
    },
  },
  {
    name: "register",
    path: "/register",
    component: Register,
    meta: {
      guest: true,
    },
  },
  {
    name: "home",
    path: "/home",
    component: Home,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "lottery",
    path: "/lottery",
    component: Lottery,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "lotteryplay",
    path: "/lottery/:group/:type/:id",
    component: LottoPlay,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "createfavorite",
    path: "/favorite/create",
    component: Favorite,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "pingpong",
    path: "/pingpong",
    component: Pingpong,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "pingpongplay",
    path: "/pingpong/:id",
    component: PingpongPlay,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "deposit",
    path: "/deposit",
    component: Deposit,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "withdraw",
    path: "/withdraw",
    component: Withdraw,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "transection",
    path: "/transection",
    component: Transection,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "lottoview",
    path: "/lottoview",
    component: LotteryView,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "awardresult",
    path: "/award",
    component: Awardresult,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: "profile",
    path: "/profile",
    component: Profile,
    meta: {
      requiresAuth: true,
    },
  },
];

const router = new VueRouter({
  mode: "history",
  routes: routes,
});
router.beforeEach((to, _from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const isLoggedIn = store.getters["auth/isLoggedIn"];
    if (isLoggedIn) {
      next();
      return;
    } else {
      next({ path: "/signin", params: { nextUrl: to.fullPath } });
    }
  } else {
    next();
  }
});

export default router;
