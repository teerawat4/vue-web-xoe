const { urlwithdraw } = require("./utils");
const axios = require("axios");
const accountDetails = async (userid, callback) => {
  try {
    const packages = await fetch(
      `${urlwithdraw}?type=showaccountdetail&memberId=${userid}`
    );
    const { account } = await packages.json();
    return callback(account[0]);
  } catch (error) {
    console.log(error);
  }
};

const getshowAccountDetail = async (memberId) => {
  try {
    const accountmember = await fetch(
      `${urlwithdraw}?type=showaccountdetail&memberId=${memberId}`,
      {
        methods: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    const { account } = accountmember.data;
    return account;
  } catch (error) {
    console.log(error);
  }
};

const getWithdraw = async (memberId, callback) => {
  try {
    const withdraw = await axios.get(
      `${urlwithdraw}?type=showwithdraw&memberId=${memberId}`
    );
    const { withdraws } = withdraw.data;
    return callback(withdraws);
  } catch (error) {
    console.log(error);
  }
};

const cancelWithdraw = async (memberId, requestId, $username) => {
  try {
    const cancel = await fetch(
      `${urlwithdraw}?type=cancelWidthdraw&memberId=${memberId}&requestId=${requestId}&username=${$username}`,
      {
        methods: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    const { cancelWithdraw } = cancel.data;
    return cancelWithdraw;
  } catch (error) {
    console.log(error);
  }
};

const createWithdraw = async (memberId, amount, requestNote, ip) => {
  try {
    console.log(memberId, amount, requestNote, ip);
    const params = new URLSearchParams();
    params.append("type", "requestwithdraw");
    params.append("memberId", memberId);
    params.append("amount", amount);
    params.append("requestNote", requestNote);
    params.append("REMOTE_ADDR", ip);
    params.append("withdrawlimit", 6);
    const request = await axios.post(urlwithdraw, params);
    console.log(request);
    const { requestwithdraw } = request.data;
    return requestwithdraw;
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  accountDetails,
  getshowAccountDetail,
  getWithdraw,
  cancelWithdraw,
  createWithdraw,
};
