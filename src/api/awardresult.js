export default {
  async lottoGov(callback) {
    try {
      //หวยรัฐบาล
      const lottoGov = await fetch(
        "https://new.pan600.com/Api/lib/actionLottoGame.php?type=getGov"
      );
      const { govs } = await lottoGov.json();
      callback(govs);
    } catch (error) {
      console.log(error);
    }
  },
  async lottoStock(callback) {
    try {
      //หวยหุ้นต่างประเทศ
      const lottoStock = await fetch(
        "https://new.pan600.com/Api/lib/actionLottoGame.php?type=getStock"
      );
      const { tickets } = await lottoStock.json();
      callback(tickets);
    } catch (error) {
      console.log(error);
    }
  },
  async lottoLoasandVianam(callback) {
    try {
      //ลาวและเวียดนาม
      const lottoStock = await fetch(
        "https://new.pan600.com/Api/lib/actionLottoGame.php?type=getloasVianam"
      );
      const { loas } = await lottoStock.json();
      callback(loas);
    } catch (error) {
      console.log(error);
    }
  },
  async lottoPingpongs(callback) {
    try {
      //ลาวและเวียดนาม
      const lottoStock = await fetch(
        "https://new.pan600.com/Api/lib/actionLottoGame.php?type=getpingpong"
      );
      const { pingpong } = await lottoStock.json();
      let countItem = pingpong.length;
      pingpong.forEach((element) => {
        element.index = countItem;
        countItem--;
      });
      callback(pingpong);
    } catch (error) {
      console.log(error);
    }
  },
};
