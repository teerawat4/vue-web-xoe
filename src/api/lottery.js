import { urlLottory } from "./utils";

export default {
  async getLottos(callback) {
    try {
      const allGames = await fetch(`${urlLottory}type=getselectGroup`);
      const { request } = await allGames.json();
      return callback(request);
    } catch (error) {
      console.log(error);
    }
  },
  async getLottoPacket(callback) {
    try {
      const packages = await fetch(
        `https://new.pan600.com/Api/lib/actionLotto.php?type=lottoinId&priceIds[]=1,15,16`
      );
      const { lottos } = await packages.json();
      return callback(lottos);
    } catch (error) {
      console.log(error);
    }
  },
  async getCredit(userid, callback) {
    try {
      const packages = await fetch(
        `https://new.pan600.com/Api/lib/actionLottoGame.php?type=getCredit&memberId=${userid}`
      );
      const { credit } = await packages.json();
      return callback(credit);
    } catch (error) {
      console.log(error);
    }
  },
  fillterLottos(lotto) {
    const { games } = lotto;
    const gov = games[0];
    const { before, after } = gov.games;
    gov.game_date = before.game_date;
    gov.close_time = before.close_time;
    gov.result = after;
    return gov;
  },
};
