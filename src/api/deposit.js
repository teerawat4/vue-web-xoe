const axios = require("axios");
const { urldeposit } = require("./utils");

const depositForm = async (memberId) => {
  try {
    const forms = await axios.get(
      `${urldeposit}type=showdeposit&memberId=${memberId}`
    );
    const { deposits } = forms.data;
    return deposits;
  } catch (error) {
    console.log(error);
  }
};

const reqBankAccount = async (memberId, amount) => {
  // console.log(memberId, amount);
  try {
    const formdata = await axios.get(
      `${urldeposit}type=showform&memberId=${memberId}&amount=${amount}`
    );
    const { forms } = formdata.data;
    // console.log(formdata);
    return forms;
  } catch (error) {
    console.log(error);
  }
};

const createDeposit = async (
  memberId,
  depositDate,
  amount,
  requestNote,
  ipAddress
) => {
  const params = new URLSearchParams();
  params.append("memberId", memberId);
  params.append("depositDate", depositDate);
  params.append("amount", amount);
  params.append("requestNote", requestNote);
  params.append("REMOTE_ADDR", ipAddress);

  const createDeposit = await axios.post(`${urldeposit}type=request`, params);
  const { request } = createDeposit.data;
  return request;
};

const cancelDeposit = async (memberId, requestId, username) => {
  try {
    const cancels = await axios.put(
      `${urldeposit}type=cancel&memberId=${memberId}&username=${username}&requestId=${requestId}`
    );
    const { cancel } = cancels.data;
    return cancel;
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  createDeposit,
  reqBankAccount,
  depositForm,
  cancelDeposit,
};
