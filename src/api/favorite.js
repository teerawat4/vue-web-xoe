import { urlfavorite } from "@/api/utils";
export default {

  async createFavorite(payload, callback) {
    //สร้างโพย
    try {
      const create = await fetch(`${urlfavorite}type=createFavorite`, {
        method: "post",
        body: payload,
      });
      const res = await create.json();
      return callback(res);
    } catch (error) {
      console.log(error);
    }
  },
 
};
