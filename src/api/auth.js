import { urlLogin } from "./utils";
export default {
  async isLoggedIn(users, callback) {
    try {
      const result = await fetch(`${urlLogin}vuelogin`, {
        method: "post",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: users,
      });
      const { user } = await result.json();
      if (user.status == true) {
        // const liffId = users.get("liffId");
        const resIp = await fetch("https://api.ipify.org/?format=json");
        const { ip } = await resIp.json();
        // localStorage.setItem("liffId", liffId);
        localStorage.setItem("userId", user.memberId);
        localStorage.setItem("userName", user.userId);
        localStorage.setItem("name", user.memberName);
        localStorage.setItem("ip", ip);
      }
      return callback(user);
    } catch (error) {
      console.log(error);
    }
  },
  //   async isCheckOnline(users, callback) {
  //     const user = new URLSearchParams();
  //     user.append("username", users.userId);
  //     user.append("liffId", users.liffId);
  //     try {
  //       const result = await fetch(`${urlSignin}checkliff`, {
  //         method: "post",
  //         headers: {
  //           "Content-Type": "application/x-www-form-urlencoded",
  //         },
  //         body: user,
  //       });
  //       const { status } = await result.json();
  //       return callback(status);
  //     } catch (error) {
  //       console.log(error);
  //     }
  //   },
};
