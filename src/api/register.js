import { urlLogin } from "./utils";
import axios from "axios";
export default {
  async register(users, callback) {
    try {
      const result = await axios.post(`${urlLogin}register/vue`, users, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      });
      const user = result.data;
      if (user.status == true) {
        /*  */
        // const liffId = users.get("liffId");
        const resIp = await fetch("https://api.ipify.org/?format=json");
        const { ip } = await resIp.json();
        // localStorage.setItem("liffId", liffId);
        localStorage.setItem("userId", user.access.memberId);
        localStorage.setItem("userName", user.access.userId);
        localStorage.setItem("name", user.access.memberName);
        localStorage.setItem("ip", ip);
      }
      return callback(user);
    } catch (error) {
      console.log(error);
    }
  },
};
