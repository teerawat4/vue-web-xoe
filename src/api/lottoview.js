const axios = require("axios");
const { urllottoview } = require("./utils");

const getHistory = async (date, callback) => {
  try {

    const forms = await axios.get(
      `${urllottoview}type=hisTicket&memberId=${date.userId}&start=${date.start}&end=${date.end}`
    );
    const { tickets } = forms.data;
    // return tickets;
    // console.log(urllottoview)
    callback(tickets)
  } catch (error) {
    console.log(error);
  }
};
const getReject = async (data, callback) => {
    try {
      const forms = await axios.get(
        `${urllottoview}type=rejectTicket&ticketId=${data.id}&memberId=${data.userId}`
      );
      const { tickets } = forms.data;
      // return tickets;
      // console.log(urllottoview)
      callback(tickets)
    } catch (error) {
      console.log(error);
    }
  };
  const getDetail = async (data, callback) => {
    try {
      const forms = await axios.get(
        `${urllottoview}type=getTicket&ticketId=${data.id}`
      );
      const { tickets } = forms.data;
      // return tickets;
      // console.log(urllottoview)
      callback(tickets)
    } catch (error) {
      console.log(error);
    }
  };
  const getPayment = async (data, callback) => {
    try {
      const forms = await axios.get(
        `${urllottoview}type=getPayamount&ticketId=${data.id}`
      );
      const { payamount } = forms.data;
      // return tickets;
      // console.log(urllottoview)
      callback(payamount)
    } catch (error) {
      console.log(error);
    }
  };
  
module.exports = {
  getHistory,
  getReject,
  getDetail,
  getPayment
};
