const axios = require("axios");

const getProfile = async (userId, callback) => {
  // request user profile from newpan600
  const options = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  };
  // console.log(userId);
  const params = new URLSearchParams();
  params.append("type", "checkUserid");
  params.append("user_id", userId);
  params.append("os", "line liff");
  params.append("user_ip", "127.0.0.1");
  try {
    const profile = await axios.post(
      "https://new.pan600.com/Api/lib/actionLogin.php",
      params,
      options
    );
    return callback(profile.data);
  } catch (err) {
    console.log(err);
  }
};

const editProfile = async (profileData, callback) => {
  const options = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  };
  try {
    const update = await axios.post(
      "https://new.pan600.com/Api/lib/actionRegister.php",
      profileData,
      options
    );
    // console.log(update.data);
    return callback(update.data);
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  getProfile,
  editProfile,
};
