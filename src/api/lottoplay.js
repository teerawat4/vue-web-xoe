import { newVersionObject } from "./objlotto";
import { urlactionLotto } from "@/api/utils";
export default {
  async getLottoPriceId(payload, callback) {
    try {
      //ดึง แพ็คเก็จหวย
      const packages = await fetch(
        `https://new.pan600.com/Api/lib/actionLotto.php?type=getpriceid&memberId=194&lotto_group_code=${payload.group}`
      );
      const { game } = await packages.json();
      return callback(game);
    } catch (error) {
      console.log(error);
    }
  },
  async getLottoPacket(priceId, callback) {
    try {
      //ดึง แพ็คเก็จหวย
      const packages = await fetch(
        `https://new.pan600.com/Api/lib/actionLotto.php?type=lottoinId&priceIds[]=${priceId[0]},${priceId[1]},${priceId[2]}`
      );
      const { lottos } = await packages.json();
      return callback(lottos);
    } catch (error) {
      console.log(error);
    }
  },
  async getfullbetofGame(gameId, callback) {
    //ดึงเลขเต็ม
    try {
      //ดึง แพ็คเก็จหวย
      const fullbetNumber = await fetch(
        `https://new.pan600.com/Api/lib/actionLotto.php?type=getfullbermember&game_id=${gameId}`
      );
      const { fullbetmember } = await fullbetNumber.json();
      return callback(fullbetmember);
    } catch (error) {
      console.log(error);
    }
  },
  convertFullpredicts(item) {
    let newArray = item;
    let newItemsplay = [
      { name: "3 ตัว", items: [], limit: 3 },
      { name: "2 ตัว", items: [], limit: 2 },
      { name: "1 ตัว", items: [], limit: 1 },
    ];
    newArray.lotto_for_id.forEach((element) => {
      //นำมาแยกประเภท
      if (
        element.lotto_bet == "3 บน" ||
        element.lotto_bet == "3 โต๊ด" ||
        element.lotto_bet == "3 ล่าง"
      ) {
        newVersionObject.map((pre, index) => {
          pre.item.filter((row) => {
            if (row.code == element.lotto_bet_code) {
              row.payrate = element.pay_rate;
              row.discount = element.discount_rate;
              newItemsplay[index].items.push(row);
              newItemsplay[index].items.sort((a, b) =>
                a.index > b.index ? 1 : -1
              );
            }
          });
        });
      } else if (
        element.lotto_bet == "2 บน" ||
        element.lotto_bet == "2 โต๊ด" ||
        element.lotto_bet == "2 ล่าง"
      ) {
        newVersionObject.map((pre, index) => {
          pre.item.filter((row) => {
            if (row.code == element.lotto_bet_code) {
              row.payrate = element.pay_rate;
              row.discount = element.discount_rate;
              newItemsplay[index].items.push(row);
              newItemsplay[index].items.sort((a, b) =>
                a.index > b.index ? 1 : -1
              );
            }
          });
        });
      } else if (
        element.lotto_bet == "3 บน" ||
        element.lotto_bet == "วิ่ง ล่าง" ||
        element.lotto_bet == "วิ่ง บน" ||
        element.lotto_bet == "2 บน"
      ) {
        newVersionObject.map((pre, index) => {
          pre.item.filter((row) => {
            if (row.code == element.lotto_bet_code) {
              row.payrate = element.pay_rate;
              row.discount = element.discount_rate;
              newItemsplay[index].items.push(row);
              newItemsplay[index].items.sort((a, b) =>
                a.name > b.name ? 1 : -1
              );
            }
          });
        });
      }
    });
    newArray.isplay = newItemsplay;
    return newArray;
  },
  async createTicket(payload, callback) {
    //สร้างโพย
    try {
      const create = await fetch(`${urlactionLotto}type=checkfunedit`, {
        method: "post",
        body: payload,
      });
      const res = await create.json();
      return callback(res);
    } catch (error) {
      console.log(error);
    }
  },
  async getFavorite(memberId, callback) {
    //โพยโปรด
    try {
      //ดึง แพ็คเก็จหวย
      const fullbetNumber = await fetch(
        `https://new.pan600.com/Api/lib/actionFavorite.php?type=getFavorite&memberId=${memberId}`
      );
      const { request } = await fullbetNumber.json();
      return callback(request);
    } catch (error) {
      console.log(error);
    }
  },
  async getFavoriteWithId(favoriteid, callback) {
    //โพยโปรด
    try {
      //ดึง โพยโปรด ด้วย id โพยโปรด
      const favoriteId = await fetch(
        `https://new.pan600.com/Api/lib/actionFavorite.php?type=FavoriteDetail&favoriteId=${favoriteid}`
      );
      const { request } = await favoriteId.json();
      return callback(request);
    } catch (error) {
      console.log(error);
    }
  },
  async getTicketRepeat(memberId, gameId, callback) {
    try {
      //ดึง แพ็คเก็จหวย
      const ticketRepeats = await fetch(
        `https://new.pan600.com/Api/lib/actionFavorite.php?type=getLottoBetBill&memberId=${memberId}&gameId=${gameId}`
      );
      const { request } = await ticketRepeats.json();
      return callback(request);
    } catch (error) {
      console.log(error);
    }
  },
  async getTicketRepeatWithId(payload, callback) {
    //โพยโปรด
    try {
      //ดึง โพยโปรด ด้วย id โพยโปรด
      const favoriteId = await fetch(
        `https://new.pan600.com/Api/lib/actionFavorite.php?type=getNumberofTicket&memberId=${payload.userId}&gameId=${payload.gameId}&ticketId=${payload.ticketId}`
      );
      const { request } = await favoriteId.json();
      return callback(request);
    } catch (error) {
      console.log(error);
    }
  },
  async getDeleteFavirote(ticketId, callback) {
    //โพยโปรด
    try {
      //ดึง โพยโปรด ด้วย id โพยโปรด
      const favoriteId = await fetch(
        `https://new.pan600.com/Api/lib/actionFavorite.php?type=deleteFavorite&favoriteId=${ticketId}`
      );
      const { request } = await favoriteId.json();
      return callback(request);
    } catch (error) {
      console.log(error);
    }
  },
};
