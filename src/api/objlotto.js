export const objAmount = [
  //objAmount เป็น array convert type จากหลังบ้านที่ส่งมา เป็น t, b tb, 100x 100*100
  {
    chamount: "x",
    type: "t",
    typename: "2 โต๊ด",
    look: "tod",
    limit: 2,
    lotto_bet: "2 โต๊ด",
    desc: "",
    visible: false,
    betcode: "top2",
  },
  {
    chamount: "x",
    type: "t",
    typename: "3 โต๊ด",
    look: "tod",
    limit: 3,
    lotto_bet: "3 โต๊ด",
    desc: "",
    visible: false,
    betcode: "top3",
  },
  {
    chamount: "t2",
    type: "t",
    typename: "2 ตัวบน + กลับ",
    look: "t2",
    limit: 2,
    lotto_bet: "2 บน",
    desc: "",
    visible: false,
    betcode: "top2",
  },
  {
    chamount: "b2",
    type: "b",
    typename: "2 ตัวล่าง + กลับ",
    look: "b2",
    limit: 2,
    lotto_bet: "2 บน",
    desc: "",
    visible: false,
    betcode: "bot2",
  },
  {
    chamount: "tb4",
    type: "tb",
    typename: "2 บน + 2 ล่าง และ กลับ",
    look: "tb4",
    limit: 2,
    lotto_bet: "2 บน",
    desc: "",
    visible: false,
    betcode: "top2",
  },
  {
    chamount: "19x",
    type: "t",
    typename: "19 หางบน",
    look: "19tail",
    limit: 1,
    lotto_bet: "2 บน",
    desc: "",
    visible: false,
    betcode: "top2",
  },
  {
    chamount: "19x",
    type: "b",
    typename: "19 หางล่าง",
    look: "19tail",
    limit: 1,
    lotto_bet: "2 ล่าง",
    desc: "",
    visible: false,
    betcode: "bot2",
  },
  {
    chamount: "10x",
    type: "t",
    typename: "ปักหลักสิบ บน",
    look: "pin",
    limit: 1,
    lotto_bet: "2 บน",
    desc: "",
    visible: false,
    betcode: "top2",
  },
  {
    chamount: "10x",
    type: "b",
    typename: "ปักหลักสิบ ล่าง",
    look: "pin",
    limit: 1,
    lotto_bet: "2 ล่าง",
    desc: "",
    visible: false,
    betcode: "bot2",
  },
  {
    chamount: "100x",
    type: "t",
    typename: "ปักหลักร้อย บน",
    look: "pin100",
    limit: 1,
    lotto_bet: "3 บน",
    desc: "",
    visible: false,
    betcode: "top3",
  },
  {
    chamount: "100x",
    type: "b",
    typename: "ปักหลักร้อย ล่าง",
    look: "pin100",
    limit: 1,
    lotto_bet: "3 ล่าง",
    desc: "",
    visible: false,
    betcode: "top3",
  },
];
export const objNumber = [
  {
    chamount: "t6",
    type: "t",
    typename: "3 บน  3 กลับ และ 6 กลับ",
    look: "t6",
    limit: 3,
    lotto_bet: "3 บน",
    desc: "",
    visible: false,
    betcode: "top3",
  },
  {
    chamount: "b6",
    type: "b",
    typename: "3 ล่าง  3 กลับ และ 6 กลับ",
    look: "b6",
    limit: 3,
    lotto_bet: "3 ล่าง",
    desc: "",
    visible: false,
    betcode: "bot3",
  },
  {
    chamount: "tb6",
    type: "tb",
    typename: "3 บน + 3 ล่าง และ กลับ",
    look: "tb6",
    limit: 3,
    lotto_bet: "3 บน",
    desc: "",
    visible: false,
    betcode: "top3",
  },
  {
    chamount: "xx",
    type: "t",
    typename: "เบิ้ลบน",
    look: "xx",
    limit: 2,
    lotto_bet: "2 บน",
    desc: "*เลขเบิ้ล จะเป็นตัวเลข 00-99  จำนวน 10 ชุด",
    visible: false,
    betcode: "top2",
  },
  {
    chamount: "xx",
    type: "b",
    typename: "เบิ้ลล่าง",
    look: "xx",
    limit: 2,
    lotto_bet: "2 ล่าง",
    desc: "*เลขเบิ้ล จะเป็นตัวเลข 00-99  จำนวน 10 ชุด",
    visible: false,
    betcode: "bot2",
  },
  {
    chamount: "xxx",
    type: "t",
    typename: "ตองบน",
    look: "xxx",
    limit: 3,
    lotto_bet: "3 บน",
    desc: "*เลขตอง จะเป็นตัวเลข 000-999 จำนวน 10 ชุด",
    visible: false,
    betcode: "top3",
  },
];
export const objNormal = [
  {
    type: "t",
    typename: "วิ่งบน",
    look: "t",
    limit: 1,
    lotto_bet: "วิ่ง บน",
    desc: "",
    visible: false,
    betcode: "top1",
  },
  {
    type: "b",
    typename: "วิ่งล่าง",
    look: "b",
    limit: 1,
    lotto_bet: "วิ่ง บน",
    desc: "",
    visible: false,
    betcode: "bot1",
  },
  {
    type: "t",
    typename: "2 บน",
    look: "t",
    limit: 2,
    lotto_bet: "2 บน",
    desc: "",
    visible: false,
    betcode: "top2",
  },
  {
    type: "b",
    typename: "2 ล่าง",
    look: "b",
    limit: 2,
    lotto_bet: "2 ล่าง",
    desc: "",
    visible: false,
    betcode: "bot2",
  },
  {
    type: "tb",
    typename: "2 บน + 2 ล่าง ",
    look: "tb2",
    limit: 2,
    lotto_bet: "2 บน",
    desc: "",
    visible: false,
    betcode: "top2",
  },
  {
    type: "t",
    typename: "3 บน",
    look: "t",
    limit: 3,
    lotto_bet: "3 บน",
    visible: false,
    betcode: "top3",
  },
  {
    type: "b",
    typename: "3 ล่าง",
    look: "b",
    limit: 3,
    lotto_bet: "3 ล่าง",
    desc: "",
    visible: false,
    betcode: "bot3",
  },
  {
    type: "tb",
    typename: "3 บน + 3 ล่าง",
    look: "tb3",
    limit: 3,
    lotto_bet: "3 บน",
    desc: "",
    visible: false,
    betcode: "top3",
  },
];
export const arrayItem = [
  { type: "t", typename: "สามตัวบน" },
  { type: "t", typename: "สามตัวโต้ด", look: "tod" },
  { type: "t", typename: "สามตัวหน้า" },
  { type: "t", typename: "สามตัวล่าง" },
  { type: "t", typename: "+สองตัวบน" },
  { type: "t", typename: "+สองตัวล่าง" },
  { type: "t", typename: "+กลับสามตัว" },
  { type: "t", typename: "+กลับสองตัว" },
  { type: "t", typename: "วิ่งบน" },
  { type: "t", typename: "วิ่งล่าง" },
];

export const newVersionObject = [
  {
    limitname: "3 ตัว",
    limit: 3,
    item: [
      {
        type: "t",
        typename: "บน",
        lottoBet: "3 บน",
        code: "top3",
        name: "สามตัวบน",
        look: "t",
        index:1,
      },
      {
        type: "b",
        typename: "ล่าง",
        lottoBet: "3 ล่าง",
        code: "bot3",
        name: "สามตัวล่าง",
        look: "b",
        index:2,
      },
      {
        type: "t",
        typename: "3 โต๊ด",
        lottoBet: "3 โต๊ด",
        code: "top3t",
        name: "สามตัวโต๊ด",
        look: "tod",
        index:3,
      },
      {
        type: "back",
        typename: "3 กลับ",
        lottoBet: "3 บน",
        code: "top3",
        name: "สามตัวกลับ",
        look: "t",
        index:4,
      },
    ],
  },
  {
    limitname: "2 ตัว",
    limit: 2,
    item: [
      {
        type: "t",
        typename: "บน",
        lottoBet: "2 บน",
        code: "top2",
        name: "สองตัวบน",
        look: "t",
        index:1,
      },
      {
        type: "b",
        typename: "ล่าง",
        lottoBet: "2 ล่าง",
        code: "bot2",
        name: "สองตัวล่าง",
        look: "b",
        index:2,
      },
      {
        type: "t",
        typename: "2 โต๊ด",
        lottoBet: "2 โต๊ด",
        code: "top2t",
        name: "สองตัวโต๊ด",
        look: "tod",
        index:3,
      },
      {
        type: "back",
        typename: "2 กลับ",
        lottoBet: "2 บน",
        code: "top2",
        name: "สองตัวกลับ",
        look: "t",
        index:4,
      },
    ],
  },
  {
    limitname: "1 ตัว",
    limit: 1,
    item: [
      {
        type: "t",
        typename: "วิ่งบน",
        lottoBet: "วิ่ง บน",
        code: "top1",
        name: "วิ่งบน",
        look: "t",
      },
      {
        type: "b",
        typename: "วิ่งล่าง",
        lottoBet: "วิ่ง ล่าง",
        code: "bot1",
        name: "วิ่งล่าง",
        look: "b",
      },
      {
        type: "t",
        typename: "19 หางบน",
        lottoBet: "2 บน",
        code: "top2",
        name: "19 หางบน",
        look: "19tail",
      },
      {
        type: "b",
        typename: "19 หางล่าง",
        lottoBet: "2 ล่าง",
        code: "bot2",
        name: "19 หางล่าง",
        look: "19tail",
      },
      {
        type: "t",
        typename: "ปักหลักสิบบน",
        lottoBet: "2 บน",
        code: "top2",
        name: "ปักหลักสิบบน",
        look: "pin10",
      },
      {
        type: "b",
        typename: "ปักหลักสิบล่าง",
        lottoBet: "2 บน",
        code: "bot2",
        name: "ปักหลักสิบล่าง",
        look: "pin10",
      },
      {
        type: "t",
        typename: "ปักหลักร้อยบน",
        lottoBet: "3 บน",
        code: "top3",
        name: "ปักหลักร้อยบน",
        look: "pin100",
      },
      {
        type: "b",
        typename: "ปักหลักร้อยล่าง",
        lottoBet: "3 บน",
        code: "bot3",
        name: "ปักหลักร้อยล่าง",
        look: "pin100",
      },
    ],
  },
];

export const isCheckAmount = (num, look) => {
  //เช็คราคา เช่น โต๊ด tod = x100 , t2 = 2 ตัวบน + กลับ , b2 = 2 ตัวล่าง + กลับ , tb4 = บนล่าง + กลับ
  let cv = num;
  switch (look) {
    case "tod":
      cv = `x${num}`;
      break;

    case "pin100":
      cv = `100x${num}`;
      break;

    default:
      break;
  }
  return cv;
};

export const isChecktype = (obj, num) => {
  //เช็คประเภทของหวย เช่น  3 กลับ 6 กลับ , xx = 2 ตอง , xxx = 3 ตอง , tb6 = 3 บน 3 ล่าง และ 6 กลับ
  const { type } = obj;
  let cv = num;
  switch (type) {
    case "xx":
      cv = "xx"; // เบิ้ล
      break;

    case "xxx":
      cv = "xxx"; //ตอง
      break;

    default:
      break;
  }
  return cv;
};
