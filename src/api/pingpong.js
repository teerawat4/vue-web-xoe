const axios = require("axios");
const { urlpingpong } = require("./utils");

const getinstallment = async (callback) => {
  try {
    const installments = await axios({
      url: `${urlpingpong}type=installmentpingpong`,
      method: "get",
    });
    return callback(installments.data);
  } catch (error) {
    console.log(error);
  }
};

const getissuedinstallment = async (callback) => {
  try {
    const issueinstallments = await axios({
      url: `${urlpingpong}type=issuedinstallmentpingpong`,
      method: "get",
    });
    return callback(issueinstallments.data);
  } catch (error) {
    console.log(error);
  }
};

const getuserOnline = async () => {
  try {
    const useronlines = await axios({
      url: `${urlpingpong}type=online`,
      method: "get",
    });

    return useronlines.data;
  } catch (error) {
    console.log(error);
  }
};

const getstatepost = async (gameid) => {
  try {
    const useronlines = await axios({
      url: `${urlpingpong}type=getgame&gameid=${gameid}`,
      method: "get",
    });

    return useronlines.data;
  } catch (error) {
    console.log(error);
  }
};

const getposts = async (gameid) => {
  try {
    const posts = await axios({
      url: `${urlpingpong}type=posts&gameid=${gameid}`,
      method: "get",
    });

    return posts.data;
  } catch (error) {
    console.log(error);
  }
};

const createPost = async (payload) => {
  try {
    const create = await axios.post(urlpingpong, payload);
    return create.data;
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getinstallment,
  getissuedinstallment,
  getuserOnline,
  getposts,
  getstatepost,
  createPost,
};
