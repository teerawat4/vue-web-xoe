import {
  isChecktype,
  isCheckAmount,
  objNormal,
  objAmount,
  objNumber,
} from "@/api/objlotto";
export default {
  fnPermutations(str) {
    //โต๊ด และ 3 กลับ 6 กลับ
    if (str.length == 1) return str;
    var perms = [];
    var end = str.substring(1);
    var key;
    for (key in this.fnPermutations(end)) {
      var perm;
      perm = this.fnPermutations(end)[key];
      var len;
      len = perm.length;
      var c;
      c = 0;
      while (c <= len) {
        var fh;
        fh = perm.substring(0, c);
        var m;
        m = str.substring(0, 1);
        var lh;
        lh = perm.substring(c);
        var p;
        p = `${fh}${m}${lh}`;
        let resultIndex = perms.indexOf(p);
        if (resultIndex < 0) {
          perms.push(p);
        }
        c++;
      }
    }
    return perms.sort((a, b) => b - a);
  },
  fnaddNumber(changeTypeNumber, inputNumber, numBers) {
    let newNumbers = [];
    newNumbers = numBers;
    let backNumber = changeTypeNumber
      .map(function(e) {
        return e.type;
      })
      .indexOf("back");
    changeTypeNumber.map((row) => {
      if (
        backNumber >= 0 &&
        inputNumber != "height" &&
        inputNumber != "low" &&
        inputNumber != "odd" &&
        inputNumber != "even" &&
        inputNumber != "double" &&
        inputNumber != "xxx" &&
        row.look != "tod"
      ) {
        //กลับเลข
        //row.type != "back" คือ ไม่นำ ชนิดของเลขกลับมาเข้า array
        if (row.type != "back") {
          //ตรวจสอบอินเด็กของ ออฟเจ็ค
          let resultBackNumber = this.fnPermutations(inputNumber);
          resultBackNumber.forEach((number) => {
            /* loop insert เลขกลับ */
            let resultIndex = newNumbers
              .map(function(e) {
                return e.name;
              })
              .indexOf(row.name);
            if (resultIndex >= 0) {
              newNumbers[resultIndex].numbers.splice(0, 0, number);
              newNumbers[resultIndex].numberConvert.splice(0, 0, number);
              newNumbers[resultIndex].prices.splice(0, 0, 1);
              newNumbers[resultIndex].amountConvert.splice(0, 0, 1);
            } else {
              const newPayload = {
                ...row,
                numbers: [number],
                numberConvert: [number],
                prices: [1],
                amountConvert: [1], //แปลงดาต้าเพื่อลง
              };
              newNumbers.push(newPayload);
            }
            /*  */
          });
        }
      } else {
        if (row.look == "19tail") {
          /* 19หาง */
          let result19tails = this.fn19Tails(inputNumber);
          result19tails.forEach((number) => {
            /* loop insert 19หาง */
            let resultIndex = newNumbers
              .map(function(e) {
                return e.name;
              })
              .indexOf(row.name);
            if (resultIndex >= 0) {
              newNumbers[resultIndex].numbers.splice(0, 0, number);
              newNumbers[resultIndex].numberConvert.splice(0, 0, number);
              newNumbers[resultIndex].prices.splice(0, 0, 1);
              newNumbers[resultIndex].amountConvert.splice(0, 0, 1);
            } else {
              const newPayload = {
                ...row,
                numbers: [number],
                numberConvert: [number],
                prices: [1],
                amountConvert: [1],
              };
              newNumbers.push(newPayload);
            }
            /*  */
          });
        } else if (row.look == "pin10") {
          let resultPin10 = this.fnTails10(inputNumber);
          //ปักหลัก10
          /* loop insert ปักหลักสิบ */
          resultPin10.forEach((number) => {
            /* loop insert 19หาง */
            let resultIndex = newNumbers
              .map(function(e) {
                return e.name;
              })
              .indexOf(row.name);
            if (resultIndex >= 0) {
              newNumbers[resultIndex].numbers.splice(0, 0, number);
              newNumbers[resultIndex].numberConvert.splice(0, 0, number);
              newNumbers[resultIndex].prices.splice(0, 0, 1);
              newNumbers[resultIndex].amountConvert.splice(0, 0, 1);
            } else {
              const newPayload = {
                ...row,
                numbers: [number],
                numberConvert: [number],
                prices: [1],
                amountConvert: [1],
              };
              newNumbers.push(newPayload);
            }
            /*  */
          });
          /* end ปักหลัก 10 */
        } else if (
          //เคสเลข คี่ คู่ สูง ต่ำ เบิ้ล ตอง
          inputNumber == "height" ||
          inputNumber == "low" ||
          inputNumber == "odd" ||
          inputNumber == "even" ||
          inputNumber == "double" ||
          inputNumber == "xxx"
        ) {
          if (row.look != "tod") {
            let result;
            if (inputNumber == "height" || inputNumber == "low") {
              result = this.fnHightRow(inputNumber); //สูงต่ำ
            } else if (inputNumber == "double" || inputNumber == "xxx") {
              result = this.fnDouble(inputNumber);
            } else {
              result = this.fnEven(inputNumber); //คู่ คี่
            }
            if (
              row.name == "สองตัวล่าง" ||
              (row.name == "สองตัวโต๊ด" && inputNumber != "double") ||
              row.name == "สองตัวบน" ||
              row.name == "สามตัวล่าง" ||
              row.name == "สามตัวบน"
            ) {
              let resultIndex = newNumbers
                .map(function(e) {
                  return e.name;
                })
                .indexOf(row.name);
              if (resultIndex >= 0) {
                result.number.forEach((n) => {
                  newNumbers[resultIndex].numbers.splice(0, 0, n);
                  newNumbers[resultIndex].numberConvert.splice(0, 0, n);
                  newNumbers[resultIndex].prices.splice(0, 0, 1);
                  newNumbers[resultIndex].amountConvert.splice(0, 0, n);
                });
              } else {
                const newPayload = {
                  ...row,
                  numbers: [],
                  prices: [],
                  numberConvert: [],
                  amountConvert: [],
                };
                result.number.forEach((n, index) => {
                  newPayload.numberConvert.push(n);
                  newPayload.amountConvert.push(result.price[index]);
                  newPayload.numbers.push(n);
                  newPayload.prices.push(result.price[index]);
                });

                newNumbers.push(newPayload);
              }
            }
          }
          //ออก เคสเลข คี่ คู่ สูง ต่ำ
        } else {
          //เคสปกติ
          const cvNumber = isChecktype(row, inputNumber);
          const cvPrice = isCheckAmount(1, row.look);
          let resultIndex = newNumbers
            .map(function(e) {
              return e.name;
            })
            .indexOf(row.name);
          if (resultIndex >= 0) {
            newNumbers[resultIndex].numbers.splice(0, 0, inputNumber);
            newNumbers[resultIndex].numberConvert.splice(0, 0, cvNumber);
            newNumbers[resultIndex].prices.splice(0, 0, 1);
            newNumbers[resultIndex].amountConvert.splice(0, 0, cvPrice);
            return;
          } else {
            const newPayload = {
              ...row,
              numbers: [inputNumber],
              prices: [1],
              numberConvert: [cvNumber],
              amountConvert: [cvPrice],
            };
            newNumbers.push(newPayload);
            return;
          }
        }

        //ตรวจสอบอินเด็กของ ออฟเจ็ค

        /* end if  */
      }
      /* end for each */
    });
    return newNumbers;
  },
  fn19Tails(str) {
    // 19หาง
    let tails = [];
    for (let index = 0; index < 10; index++) {
      if (index != str) tails.push(`${index}${str}`);
      tails.push(`${str}${index}`);
    }
    return tails.sort((a, b) => b - a);
  },
  fnTails10(str) {
    let tails = [];
    for (let i = 0; i < 10; i++) {
      tails.push(`${str}${i}`);
    }
    return tails.sort((a, b) => b - a);
  },
  fnTailBel(str) {
    let tails = [];
    for (let i = 0; i <= 9; i++) {
      if (str == "xx") {
        tails.push(`${i}${i}`);
      } else if (str == "xxx") {
        tails.push(`${i}${i}${i}`);
      }
    }
    return tails;
  },
  addComma(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },
  isCheckfullbet(number, array) {
    const isCheck = array.map((fullbet) => {
      if (number == fullbet.bet_numbers) return fullbet;
    });
    if (isCheck[0] == undefined) return false;
    return isCheck;
  },
  fnHightRow(type) {
    let i, numlength;
    let heightRows = [];
    let amount = [];
    const result = { number: heightRows, price: amount };
    if (type == "height") {
      i = 50;
      numlength = 99;
    } else if (type == "low") {
      i = 1;
      numlength = 49;
    }
    for (i; i <= numlength; i++) {
      if (i < 10) {
        heightRows.push(`0${i}`);
        amount.push(1);
      } else {
        heightRows.push(`${i}`);
        amount.push(1);
      }
    }
    return result;
  },
  fnEven(type) {
    let mod;
    let amount = [];
    let even = [];
    const result = { number: even, price: amount };
    if (type == "even") mod = 0;
    else if (type == "odd") mod = 1;
    for (let i = mod; i <= 99; i += 2) {
      if (i % 2 == mod) {
        if (i < 10) {
          even.push(`0${i}`);
          amount.push(1);
        } else {
          even.push(`${i}`);
          amount.push(1);
        }
      }
    }
    return result;
  },
  fnDouble(type) {
    let double = [];
    let amount = [];
    const result = { number: double, price: amount };
    //เลขเบิ้ล เลขตอง
    for (let index = 0; index <= 9; index++) {
      if (type == "double") {
        double.push(`${index}${index}`);
        amount.push(1);
      } else if (type == "xxx") {
        double.push(`${index}${index}${index}`);
        amount.push(1);
      }
    }
    return result;
  },
  convertFavorite(favorite, nNumber, packets) {
    const newtickConvert = [];
    const newArray = [];

    let newNumbers = [];
    newNumbers = nNumber;
    favorite.map((item) => {
      let { amount, numbers } = item;
      let findxAmount = amount.search("x"); //ค้นหาตำแหน่ง x
      let findxNumber = numbers.search("x"); //ค้นหาตำแหน่ง x
      let subamount = amount.substring(0, findxAmount + 1); // นำจำนวนเงินมาตัด x ออก ด้วยตำแหน่ง ที่ x + 1
      let limit = numbers.length; //เอาจำนวนเลขที่แทง มาหา ว่ามีกี่ตัว
      if (findxAmount >= 0) {
        let newAmount = amount.substring(findxAmount + 1, amount.length);
        let newIndex = objAmount.find(function(e) {
          return (
            e.chamount == subamount && e.limit == limit && e.type == item.type //ถ้าเจอให้ ให้ คืนค่า ตำแหน่งกลับไป
          );
        });
        if (newIndex) {
          //ถ้ามีค่า หรือ ถ้ามี x ที่จำนวนเงิน จะเข้าลูบนี่ แปลงค่า x เช่น x100 จะเป็น amount = 100
          newtickConvert.push({
            number: item.numbers,
            amount: newAmount,
            limit: limit,
            ...newIndex,
          });
        } else {
          //แต่ถ้า amonut ไม่มี x จะเข้า default objAmount 2 เป็น type ปกติ
          let obj = objAmount[2];
          newtickConvert.push({
            number: item.numbers,
            amount: newAmount,
            limit: limit,
            ...obj,
          });
        }
      } else if (findxNumber >= 0) {
        //กรณี number มี x เช่น 121x , 100x
        let number = numbers;

        if (limit >= 4) {
          // ถ้า number มี x จำนวนเลขจะเท่ากับ 4
          let subNumber = numbers.substring(3, 4); // ตัดตัวเลขออกเอา ค่า x มาเทียบ
          if (item.type == "t")
            //เทียบชนิด t , b , tb
            number = subNumber == "x" ? (subNumber = "t6") : subNumber;
          if (item.type == "b")
            number = subNumber == "x" ? (subNumber = "b6") : subNumber;
          if (item.type == "tb")
            number = subNumber == "x" ? (subNumber = "tb6") : subNumber;
          limit = 3;
        }
        let newAmountIndex = objNumber.find(function(e) {
          return (
            e.chamount === number && e.limit == limit && e.type == item.type
          );
        });
        if (newAmountIndex) {
          newtickConvert.push({
            number:
              number == "t6" || number == "tb6" || number == "b6"
                ? item.numbers.replace("x", "")
                : item.numbers, //เช็คว่า look == type ที่กำหนด หรือไหม ถ้าใช่ ให้ ตัด x ออก
            amount: amount,
            limit: limit,
            ...newAmountIndex,
          });
        }
      } else {
        objNormal.map((normal) => {
          if (item.numbers.length == normal.limit && item.type == normal.type) {
            newtickConvert.push({
              number: item.numbers,
              amount: item.amount,
              limit: limit,
              ...normal,
            });
          }
        });
      }
    });
    newtickConvert.forEach((row) => {
      if (row.limit == 1) {
        /* เลข 1 ตัว */
        const limit1 = packets[2];
        if (
          (row.limit == 1 && row.typename == "วิ่งบน") ||
          (row.limit == 1 && row.typename == "วิ่งล่าง")
        ) {
          // วิ่งบน และ วิ่งล่าง
          if (row.type == "t") {
            const runTop = {
              //0
              type: "t",
              typename: "วิ่งบน",
              lottoBet: "วิ่ง บน",
              code: "top1",
              name: "วิ่งบน",
              look: "t",
            };
            const resultTop2 = limit1.items.findIndex(
              (result) =>
                result.look == runTop.look &&
                result.code == runTop.code &&
                result.name == runTop.name
            );
            const newTop2 = limit1.items[resultTop2];
            const newObject = {
              ...newTop2,
            };
            newObject.number = row.number;
            newArray.push(newObject);
          } else if (row.type == "b") {
            const runBot = {
              //1
              type: "b",
              typename: "วิ่งล่าง",
              lottoBet: "วิ่ง ล่าง",
              code: "bot1",
              name: "วิ่งล่าง",
              look: "b",
            };
            const resultRunBot = limit1.items.findIndex(
              (result) =>
                result.look == runBot.look &&
                result.code == runBot.code &&
                result.name == runBot.name
            );
            if (resultRunBot >= 0) {
              //เช็คว่ามีล่างไหม
              const newTop2 = limit1.items[resultRunBot];
              const newObject = {
                ...newTop2,
              };
              newObject.number = row.number;
              newArray.push(newObject);
            }
          }
          return;
        } else if (row.chamount == "19x") {
          //19หาง บน และ 19หางล่าง
          const result19tails = this.fn19Tails(row.number);
          for (let index = 0; index < result19tails.length; index++) {
            if (row.type == "t") {
              let top = {
                //2
                type: "t",
                typename: "19 หางบน",
                lottoBet: "2 บน",
                code: "top2",
                name: "19 หางบน",
                look: "19tail",
              };
              const resultTop2 = limit1.items.findIndex(
                (result) =>
                  result.look == top.look &&
                  result.code == top.code &&
                  result.name == top.name
              );
              const newTop2 = limit1.items[resultTop2];
              const newObject = {
                ...newTop2,
              };
              newObject.number = result19tails[index];
              newArray.push(newObject);
            } else if (row.type == "b") {
              const bot = {
                type: "b",
                typename: "19 หางล่าง",
                lottoBet: "2 ล่าง",
                code: "bot2",
                name: "19 หางล่าง",
                look: "19tail",
              };
              const resultBot2 = limit1.items.findIndex(
                (result) =>
                  result.look == bot.look &&
                  result.code == bot.code &&
                  result.name == bot.name
              );
              if (resultBot2 >= 0) {
                //เช็คว่ามีล่างไหม
                const newTop2 = limit1.items[resultBot2];
                const newObject = {
                  ...newTop2,
                };
                newObject.number = result19tails[index];
                newArray.push(newObject);
              }
            }
          }
          return;
        } else if (row.chamount == "10x") {
          //ปักหลักสิบ และ ปักหลักสิบล่าง
          const result19tails = this.fnTails10(row.number);
          result19tails.forEach((num) => {
            if (row.type == "t") {
              const pin10Top = {
                //4
                type: "t",
                typename: "ปักหลักสิบบน",
                lottoBet: "2 บน",
                code: "top2",
                name: "ปักหลักสิบบน",
                look: "pin10",
              };
              const resultTop2 = limit1.items.findIndex(
                (result) =>
                  result.look == pin10Top.look &&
                  result.code == pin10Top.code &&
                  result.name == pin10Top.name
              );
              const newTop2 = limit1.items[resultTop2];
              const newObject = { ...newTop2 };
              newObject.number = num;
              newArray.push(newObject);
            } else if (row.type == "b") {
              const pin10Bot = {
                //5
                type: "b",
                typename: "ปักหลักสิบล่าง",
                lottoBet: "2 บน",
                code: "bot2",
                name: "ปักหลักสิบล่าง",
                look: "pin10",
              };
              const resultBot2 = limit1.items.findIndex(
                (result) =>
                  result.look == pin10Bot.look &&
                  result.code == pin10Bot.code &&
                  result.name == pin10Bot.name
              );
              if (resultBot2 >= 0) {
                //เช็คมี่ล่างไหม
                const newBot2 = limit1.items[resultBot2];
                const newObject = { ...newBot2 };
                newObject.number = num;
                newArray.push(newObject);
              }
            }
          });
          return;
        } else if (row.chamount == "100x") {
          //ปักหลักร้อย
          if (row.type == "t") {
            const pin100top = {
              type: "t",
              typename: "ปักหลักร้อยบน",
              lottoBet: "3 บน",
              code: "top3",
              name: "ปักหลักร้อยบน",
              look: "pin100",
            };
            const resultTop3 = limit1.items.findIndex(
              (result) =>
                result.look == pin100top.look &&
                result.code == pin100top.code &&
                result.name == pin100top.name
            );
            const newTop3 = limit1.items[resultTop3];
            const newObject = { ...newTop3 };
            newObject.number = row.number;
            newArray.push(newObject);
          } else if (row.type == "b") {
            const pin100bot = {
              type: "b",
              typename: "ปักหลักร้อยล่าง",
              lottoBet: "3 บน",
              code: "bot3",
              name: "ปักหลักร้อยล่าง",
              look: "pin100",
            };
            const resultBot3 = limit1.items.findIndex(
              (result) =>
                result.look == pin100bot.look &&
                result.code == pin100bot.code &&
                result.name == pin100bot.name
            );
            if (resultBot3 >= 0) {
              const newBot3 = limit1.items[resultBot3];
              const newObject = { ...newBot3 };
              newObject.number = row.number;
              newArray.push(newObject);
            }
          }
          return;
        }
        /*  */
      } else if (row.limit == 2) {
        const limit2 = packets[1];
        /* เลข 2 ตัว */
        const top2 = {
          type: "t",
          typename: "บน",
          lottoBet: "2 บน",
          code: "top2",
          name: "สองตัวบน",
          look: "t",
          index: 1,
        };
        const bot2 = {
          type: "b",
          typename: "ล่าง",
          lottoBet: "2 ล่าง",
          code: "bot2",
          name: "สองตัวล่าง",
          look: "b",
          index: 2,
        };
        const resultTop2 = limit2.items.findIndex(
          (result) =>
            result.look == top2.look &&
            result.code == top2.code &&
            result.name == top2.name
        );
        const resultBot2 = limit2.items.findIndex(
          (result) =>
            result.look == bot2.look &&
            result.code == bot2.code &&
            result.name == bot2.name
        );
        /*  */
        if (
          (row.type == "t" &&
            row.limit == 2 &&
            row.look != "tod" &&
            row.look != "t2") ||
          (row.type == "b" &&
            row.limit == 2 &&
            row.look != "tod" &&
            row.look != "t2")
        ) {
          //2 บน 2 ล่าง

          if (row.type == "t") {
            let newTop2 = limit2.items[resultTop2];
            const newObject = { ...newTop2 };
            newObject.number = row.number;
            newArray.push(newObject);
          } else if (row.type == "b") {
            if (resultBot2 >= 0) {
              //เช็คว่ามีล่างไหม
              let newBot2 = limit2.items[resultBot2];
              const newObject = { ...newBot2 };
              newObject.number = row.number;
              newArray.push(newObject);
            }
          }
          return;
        } else if (row.look == "tod" && row.limit == 2) {
          //2โต๊ด
          const top2tod = {
            type: "t",
            typename: "2 โต๊ด",
            lottoBet: "2 โต๊ด",
            code: "top2t",
            name: "สองตัวโต๊ด",
            look: "tod",
            index: 3,
          };
          const resultTop2Tod = limit2.items.findIndex(
            (result) =>
              result.look == top2tod.look &&
              result.code == top2tod.code &&
              result.name == top2tod.name
          );
          let newTop2 = limit2.items[resultTop2Tod];
          const newObject = { ...newTop2 };
          newObject.number = row.number;
          newArray.push(newObject);

          /*  */
          return;
        } else if (
          (row.look == "t2" && row.limit == 2) ||
          (row.look == "b2" && row.limit == 2)
        ) {
          // 2 บน กลับ และ 2 ล่าง กลับ
          const resultBack = this.fnPermutations(row.number);
          resultBack.forEach((num) => {
            let newTop2 = limit2.items[resultTop2];
            const newObject = { ...newTop2 };
            newObject.number = num;
            newArray.push(newObject);
            /*  */

            if (resultBot2 >= 0) {
              //เช็คมี 2 ล่างไหม
              let newBot2 = limit2.items[resultBot2];
              const newObject = { ...newBot2 };
              newObject.number = num;
              newArray.push(newObject);
            }
          });
          return;
        } else if (row.type == "tb" && row.limit == 2) {
          // 2 บน + 2 ล่าง

          let newTop2 = limit2.items[resultTop2];
          const newObject = { ...newTop2 };
          newObject.number = row.number;
          newArray.push(newObject);
          /*  */

          if (resultBot2 >= 0) {
            let newBot2 = limit2.items[resultBot2];
            const newObject = { ...newBot2 };
            newObject.number = row.number;
            newArray.push(newObject);
          }
          return;
        }
        /*  */
      } else if (row.limit == 3) {
        /*  */
        const limit3 = packets[0];
        if (
          (row.limit == 3 &&
            row.type == "t" &&
            row.look != "tod" &&
            row.look != "t6") ||
          (row.limit == 3 &&
            row.type == "b" &&
            row.look != "tod" &&
            row.look != "b6")
        ) {
          //3 บน หรือ 3 ล่าง
          if (row.type == "t") {
            const top3 = {
              type: "t",
              typename: "บน",
              lottoBet: "3 บน",
              code: "top3",
              name: "สามตัวบน",
              look: "t",
              index: 1,
            };
            const resultLimit3 = limit3.items.findIndex(
              (result) =>
                result.look == top3.look &&
                result.code == top3.code &&
                result.name == top3.name
            );
            let newTop3 = limit3.items[resultLimit3];
            const newObject = { ...newTop3 };
            newObject.number = row.number;
            newArray.push(newObject);
          } else if (row.type == "b") {
            const bot3 = {
              type: "b",
              typename: "ล่าง",
              lottoBet: "3 ล่าง",
              code: "bot3",
              name: "สามตัวล่าง",
              look: "b",
              index: 2,
            };
            const resultLimit3 = limit3.items.findIndex(
              (result) =>
                result.look == bot3.look &&
                result.code == bot3.code &&
                result.name == bot3.name
            );
            if (resultLimit3 >= 0) {
              //เช็คมีล่างไหม
              let newBot3 = limit3.items[resultLimit3];
              const newObject = { ...newBot3 };
              newObject.number = row.number;
              newArray.push(newObject);
            }
          }
          return;
        } else if (
          (row.limit == 3 && row.look == "t6") ||
          (row.limit == 3 && row.look == "b6")
        ) {
          //3 บนกลับ และ 3 ล่างกลับ
          const resultBlack = this.fnPermutations(row.number);
          resultBlack.forEach((num) => {
            if (row.type == "t") {
              const top3 = {
                type: "t",
                typename: "บน",
                lottoBet: "3 บน",
                code: "top3",
                name: "สามตัวบน",
                look: "t",
                index: 1,
              };
              const resultLimit3 = limit3.items.findIndex(
                (result) =>
                  result.look == top3.look &&
                  result.code == top3.code &&
                  result.name == top3.name
              );
              let newTop3 = limit3.items[resultLimit3];
              const newObject = { ...newTop3 };
              newObject.number = num;
              newArray.push(newObject);
            } else if (row.type == "b") {
              const bot3 = {
                type: "b",
                typename: "ล่าง",
                lottoBet: "3 ล่าง",
                code: "bot3",
                name: "สามตัวล่าง",
                look: "b",
                index: 2,
              };
              //
              const resultLimit3 = limit3.items.findIndex(
                (result) =>
                  result.look == bot3.look &&
                  result.code == bot3.code &&
                  result.name == bot3.name
              );
              if (resultLimit3 >= 0) {
                let newBot3 = limit3.items[resultLimit3];
                const newObject = { ...newBot3 };
                newObject.number = num;
                newArray.push(newObject);
              }
            }
          });

          return;
        } else if (row.limit == 3 && row.look == "tod") {
          // 3 โต๊ด
          const top3tod = {
            type: "t",
            typename: "3 โต๊ด",
            lottoBet: "3 โต๊ด",
            code: "top3t",
            name: "สามตัวโต๊ด",
            look: "tod",
            index: 3,
          };
          const resultLimit3 = limit3.items.findIndex(
            (result) =>
              result.look == top3tod.look &&
              result.code == top3tod.code &&
              result.name == top3tod.name
          );
          let top3Tod = limit3.items[resultLimit3];
          const newObject = { ...top3Tod };
          newObject.number = row.number;
          newArray.push(newObject);
          return;
        } else if (row.limit == 3 && row.look == "tb3") {
          //3 บน + 3 ล่าง
          const top3 = {
            type: "t",
            typename: "บน",
            lottoBet: "3 บน",
            code: "top3",
            name: "สามตัวบน",
            look: "t",
            index: 1,
          };
          const bot3 = {
            type: "b",
            typename: "ล่าง",
            lottoBet: "3 ล่าง",
            code: "bot3",
            name: "สามตัวล่าง",
            look: "b",
            index: 2,
          };
          const resultTop3 = limit3.items.findIndex(
            (result) =>
              result.look == top3.look &&
              result.code == top3.code &&
              result.name == top3.name
          );
          const resultBot3 = limit3.items.findIndex(
            (result) =>
              result.look == bot3.look &&
              result.code == bot3.code &&
              result.name == bot3.name
          );
          let top3n = limit3.items[resultTop3];
          const newObject = { ...top3n };
          newObject.number = row.number;
          newArray.push(newObject);
          //เช็คว่ามีล่างไหม
          if (resultBot3 >= 0) {
            let bot3n = limit3.items[resultBot3];
            const newObject = { ...bot3n };
            newObject.number = row.number;
            newArray.push(newObject);
          }

          return;
        } else if (row.limit == 3 && row.look == "tb6") {
          //3 บน + 3 ล่าง และ 3 กลับ
          const resultBack = this.fnPermutations(row.number);
          resultBack.forEach((num) => {
            const top3 = {
              type: "t",
              typename: "บน",
              lottoBet: "3 บน",
              code: "top3",
              name: "สามตัวบน",
              look: "t",
              index: 1,
            };
            const bot3 = {
              type: "b",
              typename: "ล่าง",
              lottoBet: "3 ล่าง",
              code: "bot3",
              name: "สามตัวล่าง",
              look: "b",
              index: 2,
            };
            /*  */
            const resultTop3 = limit3.items.findIndex(
              (result) =>
                result.look == top3.look &&
                result.code == top3.code &&
                result.name == top3.name
            );
            const resultBot3 = limit3.items.findIndex(
              (result) =>
                result.look == bot3.look &&
                result.code == bot3.code &&
                result.name == bot3.name
            );
            if (resultTop3 >= 0) {
              let top3n = limit3.items[resultTop3];
              const newObject = { ...top3n };
              newObject.number = num;
              newArray.push(newObject);
            } else if (resultBot3 >= 0) {
              //เช็คว่ามีล่างไหม
              let bot3n = limit3.items[resultBot3];
              const newObject = { ...bot3n };
              newObject.number = num;
              newArray.push(newObject);
              /*  */
            }
          });
          return;
        }
        /*  */
      }
    });

    newArray.forEach((row) => {
      //เคสปกติ
      if (row.number == "xx" || row.number == "xxx") {
        //กรณีเลขเบิ้ล
        let type;
        if (row.number == "xx") type = "double";
        else if (row.number == "xxx") type = "xxx";
        const doble = this.fnDouble(type);
        let resultIndex = newNumbers
          .map(function(e) {
            return e.name;
          })
          .indexOf(row.name);
        if (resultIndex >= 0) {
          doble.number.forEach((n) => {
            newNumbers[resultIndex].numbers.splice(0, 0, n);
            newNumbers[resultIndex].numberConvert.splice(0, 0, n);
            newNumbers[resultIndex].prices.splice(0, 0, 1);
            newNumbers[resultIndex].amountConvert.splice(0, 0, n);
          });
        } else {
          const newPayload = {
            ...row,
            numbers: [],
            prices: [],
            numberConvert: [],
            amountConvert: [],
          };
          doble.number.forEach((n, index) => {
            newPayload.numberConvert.push(n);
            newPayload.amountConvert.push(doble.price[index]);
            newPayload.numbers.push(n);
            newPayload.prices.push(doble.price[index]);
          });

          newNumbers.push(newPayload);
        }
      } else {
        const { number } = row;
        const cvNumber = isChecktype(row, number);
        const cvPrice = isCheckAmount(1, row.look);
        let resultIndex = newNumbers
          .map(function(e) {
            return e.name;
          })
          .indexOf(row.name);
        if (resultIndex >= 0) {
          newNumbers[resultIndex].numbers.splice(0, 0, number);
          newNumbers[resultIndex].numberConvert.splice(0, 0, cvNumber);
          newNumbers[resultIndex].prices.splice(0, 0, 1);
          newNumbers[resultIndex].amountConvert.splice(0, 0, cvPrice);
          return;
        } else {
          const newPayload = {
            ...row,
            numbers: [number],
            prices: [1],
            numberConvert: [cvNumber],
            amountConvert: [cvPrice],
          };
          newNumbers.push(newPayload);
          return;
        }
      }
    });

    return newNumbers;
  },
};
