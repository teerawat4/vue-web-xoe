import Vue from "vue";
import VueX, { createLogger } from "vuex";
import lottos from "./modules/lottory";
import lottoplay from "./modules/lottoplay";
import deposit from "./modules/deposit";
import auth from "./modules/authentication";
import register from "./modules/register";
import withdraw from "./modules/withdraw";
import pingpong from "./modules/pingpong";
import lottoview from "./modules/lottoview";
import transection from "./modules/transection";
import profile from "./modules/profile";
import award from "./modules/awardresult";
import favorite from "./modules/favorite";
Vue.use(VueX);

const debug = process.env.NODE_ENV !== "production";

export default new VueX.Store({
  modules: {
    auth,
    lottos,
    lottoplay,
    deposit,
    register,
    favorite,
    withdraw,
    lottoview,
    transection,
    pingpong,
    profile,
    award,
  },
  strict: false,
  plugins: debug ? [createLogger()] : [],
});
