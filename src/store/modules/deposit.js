//init state
const state = () => ({
  amounts: [],
});

//init getters
const getters = {};

//get amounts
const actions = {
  getAmount({ commit }) {
    const amount = [...state.amounts];
    commit("setAmount", amount);
  },
};

const mutations = {
  setAmount(state, amounts) {
    state.amounts = amounts;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
