import award from "@/api/awardresult";
//หน้าผลลัพท์
//init state
const state = () => ({
  govs: [],
  stocks: [],
  loas: [], //ลาวและเวียดนาม
  pingpongs: [],
  isLoading: false,
  isActive: 0,
  isPingpong: [],
});

//init getters
const getters = {
  govs: (state) => state.govs,
  stocks: (state) => state.stocks,
  loas: (state) => state.loas,
  pingpongs: (state) => state.pingpongs,
  isLoading: (state) => state.isLoading,
  isActive: (state) => state.isActive,
  isPingpong: (state) => state.isPingpong,
};

//get amounts
const actions = {
  async lottoResults({ commit }) {
    commit("isLoading");
    await award.lottoGov((res) => {
      commit("setGovs", res);
    });
    await award.lottoStock((res) => {
      commit("setStocks", res);
    });
    await award.lottoLoasandVianam((res) => {
      commit("setLoas", res);
    });
    await award.lottoPingpongs((res) => {
      commit("setPingpongs", res);
      commit("setActive", res[0].index);
    });
    commit("isLoading");
  },
};

const mutations = {
  isLoading: (state) => (state.isLoading = !state.isLoading),
  setGovs: (state, govs) => (state.govs = govs),
  setStocks: (state, stocks) => (state.stocks = stocks),
  setLoas: (state, loas) => (state.loas = loas),
  setPingpongs: (state, pingpongs) => (state.pingpongs = pingpongs),
  setActive(state, payload) {
    state.isActive = payload;
    const newActive = state.pingpongs.filter(
      (row) => row.index == state.isActive
    );
    state.isPingpong = newActive[0];
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
