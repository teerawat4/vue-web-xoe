//https://new.pan600.com/Api/lib/actionDeposit.php?type=showaccountdetail&memberId=194
import lottoview from "@/api/lottoview";
//init state
const state = () => ({
  tickets: null,
  details:null,
  rejects:null,
  pays:null,
  isLoading: false,
});

//init getters
const getters = {
  isPay:(state) => state.pays,
  isLottoview: (state) => state.tickets,
  isLoading: (state) => state.isLoading,
  isLottoReject: (state) => state.rejects,
  isLottoDetail: (state) => state.details,
};

//get amounts
const actions = {
  async getTicketHistory({ commit }, date) {
    commit("withRequest");
    await lottoview.getHistory(date, (res) => {
      commit("updateHistory", res);
      commit("withRequest");
    });
  },
  async getRejectTicket({ commit }, data) {
    commit("withRequest");
    await lottoview.getReject(data, (res) => {
      commit("rejectTicket", res);
      commit("withRequest");
    });
  },
  async getLottoDetail({ commit }, data) {
    commit("withRequest");
    await lottoview.getDetail(data, (res) => {
      commit("detailTicket", res);
      commit("withRequest");
    });
  },
  async getAllPayment({ commit }, data) {
    commit("withRequest");
    await lottoview.getPayment(data, (res) => {
      commit("allPay", res);
      commit("withRequest");
    });
  }
  
};

const mutations = {
  allPay(state, pays) {
    state.pays = pays;
  },
  updateHistory(state, tickets) {
    state.tickets = tickets;
  },
  rejectTicket(state, rejects) {
    state.rejects = rejects;
  },
  detailTicket(state, details) {
    state.details = details;
  },
  withRequest(state) {
    state.isLoading = !state.isLoading;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
