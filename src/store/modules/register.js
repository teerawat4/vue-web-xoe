import register from "@/api/register";
//init state
const state = () => ({
  isUploading: false,
});

//init getters
const getters = {
  isUploading: (state) => state.isUploading,
};

//get amounts
const actions = {
  async createSignup({ commit }, payload) {
    commit("isUploading");
    const result = await register.register(payload, (res) => {
      commit("isUploading");
      return res;
    });
    return result;
  },
};

const mutations = {
  isUploading: (state) => (state.isUploading = !state.isUploading),
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
