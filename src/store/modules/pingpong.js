import pingpong from "@/api/pingpong";
//init state
const state = () => ({
  pingpongGame: [],
  resultPingpong: [],
  posts: [],
  isLoading: false,
  isUploading: false,
  isMessage: false,
  statepost: [],
});

//init getters
const getters = {
  pingpongGame: (state) => state.pingpongGame,
  resultPingpong: (state) => state.resultPingpong,
  isLoading: (state) => state.isLoading,
  posts: (state) => state.posts,
  isUploading: (state) => state.isUploading,
  statepost: (state) => state.statepost,
};

//get amounts
const actions = {
  async requestStatePost(context, gameId) {
    context.commit("updatedIsloading");
    const state = await pingpong.getstatepost(gameId);
    if (state.status == 200) {
      context.commit("setStatePost", state.statepost[0]);
    }
    context.commit("updatedIsloading");
  },
  async requestPost(context, gameId) {
    const posts = await pingpong.getposts(gameId);
    if (posts.status == 200) {
      context.commit("setPosts", posts.posts);
    }
  },
  async requestCreatePost(context, payload) {
    const result = await pingpong.createPost(payload);
    //context.commit("updatedIsUploading");
    return result;
  },
  async requestAlredy(context, gameId) {
    const posts = await pingpong.getposts(gameId);
    const state = await pingpong.getstatepost(gameId);
    if (posts.status == 200 && state.status == 200) {
      context.commit("setStatePost", state.statepost[0]);
      context.commit("setPosts", posts.posts);
    }
  },
  async getAllPingpongGames(context) {
    context.commit("updatedLoading");
    await pingpong.getinstallment((pingpong) => {
      context.commit("setPingpong", pingpong);
      context.commit("updatedLoading");
    });
  },
  async getAllResultPingpong(context) {
    context.commit("updatedLoading");
    await pingpong.getissuedinstallment((resPingpong) => {
      context.commit("setResultPingpong", resPingpong);
      context.commit("updatedLoading");
    });
  },
};

const mutations = {
  setResultPingpong(state, resPingpong) {
    state.resultPingpong = resPingpong;
  },
  setPingpong(state, pingpong) {
    state.pingpongGame = pingpong;
  },
  setPosts(state, post) {
    state.posts = post;
  },
  setStatePost(state, statepost) {
    state.statepost = statepost;
  },
  updatedIsloading(state) {
    state.isLoading = !state.isLoading;
  },
  updatedIsUploading(state) {
    state.isUploading = !state.isUploading;
  },
  updatedLoading(state) {
    state.isLoading = !state.isLoading;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
