import lottory from "@/api/lottoplay";

//initial state
const state = () => ({
  package: [],
  selectPackage: null,
  numbers: [],
  total: 0,
  totalDiscount: 0,
  totalAll: 0,
  isLoading: false,
  isUploading: false,
  fullNumber: null,
  favorites: [],
  favoriteDetails: [],
  ticketRepeats: [],
  tickteRepeatDetails: [],
});

// getters
const getters = {
  getPackages(state) {
    return state.package;
  },
  packageSelect(state) {
    return state.selectPackage;
  },
  getNumber(state) {
    return state.numbers;
  },
  getTotal(state) {
    return state.total;
  },
  getTotalAll(state) {
    return state.totalAll;
  },
  getTotalDiscount(state) {
    return state.totalDiscount;
  },
  getisLoading(state) {
    return state.isLoading;
  },
  getisUpLoading(state) {
    return state.isUploading;
  },
  getfullNumber(state) {
    return state.fullNumber;
  },
  getFavorite: (state) => state.favorites,
  getFavoriteDetails: (state) => state.favoriteDetails,
  getTicketRepeats: (state) => state.ticketRepeats,
  getTicketRepeatsDetails: (state) => state.tickteRepeatDetails,
};

const actions = {
  async getfullbetNumber({ commit }, gameId) {
    await lottory.getfullbetofGame(gameId, (fullNumber) => {
      commit("setfullNumber", fullNumber);
    });
  },
  async getLottoPacket(context, payload) {
    //ดึงแพ็คเกจทั้งหมด
    context.commit("updatedLoading");
    context.dispatch("getfullbetNumber", payload.id);
    await lottory.getLottoPriceId(payload, async (priceId) => {
      await lottory.getLottoPacket(priceId, (packages) => {
        lottory.convertFullpredicts(packages[0]);
        context.commit("setPackets", packages);
        context.commit("updatedLoading");
      });
    });
  },
  //อัพเดทแพ็คเก็จที่เลือก
  async isUpdatePackage(context, isSelect) {
    const packageSelect = context.state.package.find((item) => {
      return item.lotto_price_id == isSelect;
    });
    context.commit("setUpdatePacket", packageSelect);
  },
  addNumber(context, numbers) {
    //เพิ่มเลขหวย
    context.commit("setNumbers", numbers);
    let newTotal = 0;
    numbers.forEach((itemName) => {
      for (let index = 0; index < itemName.prices.length; index++) {
        if (itemName.look == "pin100") {
          let pin100 = itemName.prices[index] * 100;
          newTotal += pin100;
          return;
        }
        newTotal += itemName.prices[index];
      }
    });
    //update ราคาเดิมพัน
    context.commit("updateTotal", newTotal);
  },
  clearNumber(context) {
    //ล้างค่าหวย
    context.commit("setClearNumbers");
  },
  deleteNumber(context, payload) {
    //ลบหวยทีละรายการ
    context.commit("setDeleteNumber", payload);
  },
  addPriceIndex(context, payload) {
    //เปลี่ยนราคาตามที่กรอก
    context.commit("setPriceIndex", payload);
  },
  addPricesAll(context, price) {
    //เพิ่มราคาใหม่ทั้งหมด
    context.commit("setPriceAll", price);
  },
  async submitTiacket(context, payload) {
    context.commit("updateUploading");
    const result = await lottory.createTicket(payload, (res) => {
      context.commit("updateUploading");
      return res;
    });
    return result;
  },
  async getFavorite(context, payload) {
    await lottory.getFavorite(payload.userId, async (res) => {
      context.commit("setFavorite", res);
      await lottory.getTicketRepeat(
        payload.userId,
        payload.gameId,
        (repeats) => {
          context.commit("setTicketRepeats", repeats);
        }
      );
    });
  },
  async getFavoritewithId(context, favoriteId) {
    context.commit("updateUploading");
    const res = await lottory.getFavoriteWithId(favoriteId, (res) => {
      context.commit("setfavoriteDetails", res);
      context.commit("updateUploading");
      return true;
    });
    return res;
  },
  async getTicketRepeatwithId(context, payload) {
    //ดึงโพยโปรดตามไอดีที่เลือก
    context.commit("updateUploading");
    const res = await lottory.getTicketRepeatWithId(payload, (res) => {
      context.commit("setTicketRepeatDetails", res);
      context.commit("updateUploading");
      return true;
    });
    return res;
  },
  async getDeleteFavorite(context, payload) {
    //ดึงโพยโปรดตามไอดีที่เลือก
    context.commit("updateUploading");
    const res = await lottory.getDeleteFavirote(payload, (res) => {
      context.commit("updateUploading");
      return res
    });
    return res;
  },
};

//mutations
const mutations = {
  updatedLoading(state) {
    state.isLoading = !state.isLoading;
  },
  updateUploading(state) {
    state.isUploading = !state.isUploading;
  },
  setPackets(state, packages) {
    state.package = packages;
    let payload = {
      ...packages[0], //default package
    };
    state.selectPackage = payload;
  },
  setUpdatePacket(state, newPackage) {
    let newTotal = 0;
    let newDiscount = 0;
    state.selectPackage = lottory.convertFullpredicts(newPackage);
    state.selectPackage.lotto_for_id.forEach((pack) => {
      state.numbers.filter((number) => {
        if (number.code == pack.lotto_bet_code) {
          number.payrate = pack.pay_rate;
          number.discount = pack.discount_rate;
          /*  */
          let resultDiscount = 0;
          for (let index = 0; index < number.prices.length; index++) {
            let price = number.prices[index];
            if (
              number.code == "top3t" ||
              number.code == "top2t" ||
              number.look == "pin100"
            ) {
              let findxAmount = number.amountConvert[index].search("x");
              let subamount = number.amountConvert[index].substring(
                0,
                findxAmount + 1
              );
              let result = 0;
              switch (subamount) {
                case "x":
                  result = price;
                  resultDiscount = (result * number.discount) / 100; //คิดส่วนลด
                  newTotal += result;
                  newDiscount += resultDiscount;
                  break;
                case "100x":
                  result = price * 100;
                  resultDiscount = (result * number.discount) / 100; //คิดส่วนลด
                  newTotal += result;
                  newDiscount += resultDiscount;
                  break;
                default:
                  break;
              }

              number.amountConvert[index] = `${subamount}${price}`;
            } else {
              resultDiscount = (price * number.discount) / 100;
              newDiscount += resultDiscount;
              newTotal += price;
            }
            /* end tod or pan100 */
          }
          /* end for */
        }
      });
    });
    /* end */
    state.total = newTotal;
    state.totalDiscount = newDiscount;
    state.totalAll = state.total - state.totalDiscount;
    /* update totall */
  },
  setNumbers(state, number) {
    state.numbers = number;
  },
  setClearNumbers(state) {
    state.numbers = [];
    state.total = 0;
    state.totalAll = 0;
    state.totalDiscount = 0;
  },
  setDeleteNumber(state, payload) {
    //delete position in Array
    const { code, position } = payload;
    let resultIndex = state.numbers
      .map(function(e) {
        return e.code;
      })
      .indexOf(code);
    let discount = state.numbers[resultIndex].discount; // ดึงส่วนลด
    let beforePrice = state.numbers[resultIndex].prices[position]; //ราคาเก่า
    state.totalDiscount = state.totalDiscount - (beforePrice * discount) / 100; // ลบส่วนลด
    state.total = state.total - state.numbers[resultIndex].prices[position]; // ลบเงินเดิมพัน
    state.totalAll = state.total - state.totalDiscount; //อัพเดทราคาที่ต้องชำระ
    state.numbers[resultIndex].prices.splice(position, 1); //ลบราคา
    state.numbers[resultIndex].numbers.splice(position, 1); //ลบเลข
    state.numbers[resultIndex].numberConvert.splice(position, 1); //ลบเลขที่แปลง
    state.numbers[resultIndex].amountConvert.splice(position, 1); //ลบราคาที่แปลง
    state.numbers.forEach((row, index) => {
      if (row.numbers.length <= 0 && row.numberConvert.length <= 0) {
        state.numbers.splice(index, 1);
      }
    });
  },
  setPriceIndex(state, payload) {
    const { code, price, look, position } = payload;
    let resultIndex = state.numbers.findIndex(
      (result) => result.look == look && result.code == code
    );
    let discount = state.numbers[resultIndex].discount; //ดึง แพ็คเกจส่วนลด
    let beforePrice = state.numbers[resultIndex].prices[position]; //ราคาเก่า
    let resultDiscount; //เก็บผลลัพส่วนลด
    if (code == "top3t" || code == "top2t" || look == "pin100") {
      //แก้ราคาชุดโต๊ด และ ปักหลักร้อย
      let amountX = state.numbers[resultIndex].amountConvert[position];
      let findX = amountX.search("x");
      const subAmount = amountX.substring(0, findX + 1);
      let result = 0;
      switch (subAmount) {
        case "x":
          //คิดราคา total ใหม่
          result = price;
          state.total =
            state.total - state.numbers[resultIndex].prices[position] + price;
          state.numbers[resultIndex].prices[position] = price;
          state.numbers[resultIndex].amountConvert[
            position
          ] = `${subAmount}${price}`;
          //คิดราคา ส่วนลดใหม่
          resultDiscount = (beforePrice * discount) / 100;
          state.totalDiscount = state.totalDiscount - resultDiscount;
          state.totalDiscount = state.totalDiscount + (price * discount) / 100;
          state.totalAll = state.total - state.totalDiscount;
          break;
        case "100x":
          //คิดราคา total ใหม่
          result = price * 100;
          state.total =
            state.total -
            state.numbers[resultIndex].prices[position] * 100 +
            result;
          state.numbers[resultIndex].prices[position] = price;
          state.numbers[resultIndex].amountConvert[
            position
          ] = `${subAmount}${price}`;
          //คิดราคา ส่วนลดใหม่
          resultDiscount = (beforePrice * discount) / 100;
          state.totalDiscount = state.totalDiscount - resultDiscount;
          state.totalDiscount = state.totalDiscount + (price * discount) / 100;
          state.totalAll = state.total - state.totalDiscount;
          break;
        default:
          //state.numbers[resultIndex].amountConvert[position] = price;
          break;
      }
      return;
    } else {
      state.total =
        state.total - state.numbers[resultIndex].prices[position] + price;
      //ส่วนลด
      resultDiscount = (beforePrice * discount) / 100; //คิดราคาส่วนลดเก่า
      state.totalDiscount = state.totalDiscount - resultDiscount; // ลำส่วนลดเก่ามาลบก่อน แล้วจึงนำส่วนลดใหม่ + เข้าไปแทน
      state.totalDiscount = state.totalDiscount + (price * discount) / 100;
      //อัพเดท ราคาใหม่
      state.numbers[resultIndex].prices[position] = price;
      state.numbers[resultIndex].amountConvert[position] = price;
      state.totalAll = state.total - state.totalDiscount;
    }
  },
  setPriceAll(state, newPrice) {
    //ใส่ราคาใหม่ทั้งหมด
    let newTotal = 0;
    let newDiscount = 0;
    state.numbers.forEach((itemName) => {
      let resultDiscount = 0;
      for (let index = 0; index < itemName.prices.length; index++) {
        itemName.prices[index] = newPrice;
        if (
          itemName.code == "top3t" ||
          itemName.code == "top2t" ||
          itemName.look == "pin100"
        ) {
          let findxAmount = itemName.amountConvert[index].search("x");
          let subamount = itemName.amountConvert[index].substring(
            0,
            findxAmount + 1
          );
          let result = 0;
          switch (subamount) {
            case "x":
              result = newPrice;
              resultDiscount = (result * itemName.discount) / 100; //คิดส่วนลด
              itemName.amountConvert[index] = `${subamount}${newPrice}`;
              newTotal += result;
              newDiscount += resultDiscount;
              break;
            case "100x":
              console.log(itemName);
              result = newPrice * 100;
              resultDiscount = (result * itemName.discount) / 100; //คิดส่วนลด
              itemName.amountConvert[index] = `${subamount}${newPrice}`;
              newTotal += result;
              newDiscount += resultDiscount;
              break;
            default:
              break;
          }

          itemName.amountConvert[index] = `${subamount}${newPrice}`;
        } else {
          resultDiscount = (newPrice * itemName.discount) / 100;
          itemName.amountConvert[index] = newPrice;
          newDiscount += resultDiscount;
          newTotal += newPrice;
        }
        /* end tod or pan100 */
      }
    });
    state.total = newTotal;
    state.totalDiscount = newDiscount;
    state.totalAll = state.total - state.totalDiscount;
  },
  updateTotal(state, total) {
    state.total = total;
  },
  setfullNumber(state, number) {
    state.fullNumber = number;
  },
  setFavorite(state, payload) {
    state.favorites = payload;
  },
  setfavoriteDetails(state, payload) {
    state.favoriteDetails = payload;
  },
  setTicketRepeats(state, payload) {
    state.ticketRepeats = payload;
  },
  setTicketRepeatDetails(state, payload) {
    state.tickteRepeatDetails = payload;
  },
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
