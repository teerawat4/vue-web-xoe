import isAuth from "@/api/auth";
const state = () => ({
  status: false,
  // liffId: localStorage.getItem("liffId") || "",
  userId: localStorage.getItem("userId") || "",
  userName: localStorage.getItem("userName") || "",
  name: localStorage.getItem("name") || "",
  ip: localStorage.getItem("ip") || "",
  isLoading: false,
});

//init getters
const getters = {
  getUser(state) {
    const payload = {
      // liffId: state.liffId,
      userId: state.userId,
      userName: state.userName,
      name: state.name,
      status: state.status,
      ip: state.ip,
    };
    return payload;
  },
  isLoggedIn(state) {
    if (
      state.userId != "" &&
      state.userId != undefined
      // && state.liffId != "" &&
      // state.liffId != undefined
    )
      return true;
    else return false;
  },
  isLoading: (state) => state.isLoading,
};

const actions = {
  async loggin({ commit }, user) {
    commit("authRequest");
    const res = await isAuth.isLoggedIn(user, (res) => {
      commit("authRequest");
      return res;
    });
    return res;
  },
  // async isOnline({ commit, state }) {
  //   commit("authRequest");
  //   const users = { userId: state.userName };
  //   const result = await isAuth.isCheckOnline(users, (res) => {
  //     return res;
  //   });
  //   commit("authRequest");
  //   return result;
  // },
  logout({ commit }) {
    commit("isLoggout");
  },
};

const mutations = {
  authRequest(state) {
    state.isLoading = !state.isLoading;
  },
  isLoggout(state) {
    localStorage.removeItem("userId");
    localStorage.removeItem("userName") || "",
      localStorage.removeItem("name") || "",
      localStorage.removeItem("ip") || "",
      (state.status = false);
    // state.liffId = "";
    state.userId = "";
    state.status = false;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
