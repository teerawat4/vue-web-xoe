//https://new.pan600.com/Api/lib/actionDeposit.php?type=showaccountdetail&memberId=194
import withdraw from "@/api/withdraw";
//init state
const state = () => ({
  account: null,
  showWithdraw: [],
  isLoading: false,
});

//init getters
const getters = {
  isAccount: (state) => state.account,
  isLoading: (state) => state.isLoading,
  showWithdraw: (state) => state.showWithdraw,
};

//get amounts
const actions = {
  async getAccountDetil({ commit }, userid) {
    commit("withRequest");
    await withdraw.accountDetails(userid, (res) => {
      commit("updateAccount", res);
      commit("withRequest");
    });
  },
  async getShowWithdraw({ commit }, userid) {
    await withdraw.getWithdraw(userid, (res) => {
      // console.log(res);
      commit("setShowWithdraw", res);
    });
  },
};

const mutations = {
  updateAccount(state, account) {
    state.account = account;
  },
  withRequest(state) {
    state.isLoading = !state.isLoading;
  },
  setShowWithdraw(state, showWithdraw) {
    state.showWithdraw = showWithdraw;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
