import profile from "@/api/profile";

//init state
const state = {
  profile: [],
  status: "",
};

//init getters
const getters = {
  profile: (state) => state.profile,
};

const actions = {
  async requestProfile({ commit }, userId) {
    // console.log(userId);
    await profile.getProfile(userId, (res) => {
      commit("setProfile", res);
    });
  },
  async editProfile({ commit }, payload) {
    // console.log(payload);
    await profile.editProfile(payload, (res) => {
      // console.log(res);
      commit("setEditProfileStatus", res);
    });
  },
};

const mutations = {
  setProfile(state, profiles) {
    state.profile = profiles;
  },
  setEditProfileStatus(state, status) {
    state.status = status;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
