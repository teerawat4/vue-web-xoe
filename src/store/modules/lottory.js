import lottory from "@/api/lottery";

//initial state
const state = () => ({
  lottos: [],
  gov: [],
  isLoading: false,
  credit: 0,
  isActiveBottom: localStorage.getItem("active") || "home",
});

// getters
const getters = {
  credit: (state) => state.credit,
  isActiveBottom: (state) => state.isActiveBottom,
};

//lottory.getLottos()
const actions = {
  async getAllLottos(context) {
    context.commit("updatedLoading");
    await lottory.getLottos((lottos) => {
      const gov = lottory.fillterLottos(lottos[0]);
      context.commit("setGovs", gov);
      context.commit("setLottos", lottos);
      context.commit("updatedLoading");
    });
  },
  async getCredit({ commit }, userid) {
    await lottory.getCredit(userid, (credit) => {
      commit("updatedCredit", credit);
    });
  },
  isActiveBottom({ commit }, name) {
    commit("setIsActiveBottom", name);
  },
};

//mutations
const mutations = {
  setLottos(state, lottos) {
    state.lottos = lottos;
  },
  setGovs(state, gov) {
    state.gov = gov;
  },
  updatedLoading(state) {
    state.isLoading = !state.isLoading;
  },
  updatedCredit(state, credit) {
    state.credit = credit;
  },
  setIsActiveBottom(state, name) {
    localStorage.setItem("active", name);
    state.isActiveBottom = name;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
