import favorite from "@/api/favorite";

//initial state
const state = () => ({
  isUploading: false,
  favname: ''
});

// getters
const getters = {
  getisUpLoading(state) {
    return state.isUploading;
  },
  getFavname(state) {
    return state.favname;
  },
};

const actions = {
  async submitFavorite(context, payload) {
    context.commit("updateUploading");
    const result = await favorite.createFavorite(payload, (res) => {
      context.commit("updateUploading");
      return res;
    });
    return result;
  },
  async addFavname(context, favname){
    context.commit("setFavname", favname);
  }
};

//mutations
const mutations = {
  updateUploading(state) {
    state.isUploading = !state.isUploading;
  },
  setFavname(state, favname) {
    state.favname = favname;
  }
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
