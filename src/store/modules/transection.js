//https://new.pan600.com/Api/lib/actionDeposit.php?type=showaccountdetail&memberId=194
import transection from "@/api/transection";
//init state
const state = () => ({
  tickets: null,
  details:null,
  rejects:null,
  transections:null,
  isLoading: false,
});

//init getters
const getters = {
  isTransection: (state) => state.transections,
  isLottoview: (state) => state.tickets,
  isLoading: (state) => state.isLoading,
  isLottoReject: (state) => state.rejects,
  isLottoDetail: (state) => state.details,
};

//get amounts
const actions = {
  async getTicketHistory({ commit }, date) {
    commit("withRequest");
    await transection.getHistory(date, (res) => {
      commit("updateHistory", res);
      commit("withRequest");
    });
  },
  async getRejectTicket({ commit }, data) {
    commit("withRequest");
    await transection.getReject(data, (res) => {
      commit("rejectTicket", res);
      commit("withRequest");
    });
  },
  async getLottoDetail({ commit }, data) {
    commit("withRequest");
    await transection.getDetail(data, (res) => {
      commit("detailTicket", res);
      commit("withRequest");
    });
  },
  async getDataTransection({ commit }, data) {
    commit("withRequest");
    await transection.getTransection(data, (res) => {
      commit("updateTransection", res);
      commit("withRequest");
    });
  }
};

const mutations = {
  updateTransection(state, transections) {
    state.transections = transections;
  },
  updateHistory(state, tickets) {
    state.tickets = tickets;
  },
  rejectTicket(state, rejects) {
    state.rejects = rejects;
  },
  detailTicket(state, details) {
    state.details = details;
  },
  withRequest(state) {
    state.isLoading = !state.isLoading;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
