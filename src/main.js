import Vue from "vue";
import App from "./App.vue";
import vuetify from "@/plugins/vuetify"; // path to vuetify export
import router from "./router";
import store from "./store";
import sweetalert from "vue-sweetalert2";
import browserDetect from "vue-browser-detect-plugin";

// If you don't need the styles, do not connect
import "sweetalert2/dist/sweetalert2.min.css";
import "./registerServiceWorker";

Vue.prototype.$liff = window.liff;
Vue.config.productionTip = false;
Vue.use(sweetalert);
Vue.use(browserDetect);
new Vue({
  store,
  router,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
